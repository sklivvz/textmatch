﻿using System.Collections.Generic;
using System.Linq;

namespace eviivo.Core
{
    public class StringFinder
    {
        private readonly char[] _text;

        public StringFinder(string text)
        {
            _text = text.ToUpperInvariant().ToCharArray();
        }

        /* Simple multisearch, given a search that can restart from
         * where it left. Since it uses deferred execution, it only
         * performs the search if needed, and only tries to match as
         * requested.
         */

        public IEnumerable<int> Find(string subtext)
        {
            char[] internalSubtext = subtext.ToUpperInvariant().ToCharArray();
            int scanIndex = 0;
            while (scanIndex >= 0 && scanIndex < _text.Count())
            {
                int result = NaiveFind(internalSubtext, scanIndex);
                scanIndex = result;
                if (result > 0)
                    yield return result;
            }
        }

        /* Naive substring search implementation, has O(NxM) worst case scenario, 
         * where N is the size of the text and M the size of the subtext.
         * Normally it should be closer to O(N)
         */

        private int NaiveFind(char[] subtext, int start)
        {
            int subtextIndex = 0;
            int textIndex = start;

            while (textIndex < _text.Length)
            {
                // Found a match?
                if (subtext[subtextIndex] == _text[textIndex])
                {
                    // Have we matched the whole subtext?
                    if (subtextIndex == subtext.Length - 1)
                        // the index is 1-based and points to the start of the match
                        return textIndex - subtextIndex + 1;

                    subtextIndex++;
                }
                else
                {
                    //No match, reset the text index to where we last matched
                    //Or subtract zero if we didn't match anything yet
                    textIndex -= subtextIndex;

                    subtextIndex = 0;
                }

                textIndex++;
            }
            return -1;
        }
    }
}