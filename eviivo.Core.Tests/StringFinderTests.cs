﻿using System.Linq;
using NUnit.Framework;

namespace eviivo.Core.Tests
{
    [TestFixture]
    public class StringFinderTests
    {
        [Test]
        [TestCase("aac", "a", "1,2")]
        [TestCase("Polly put the kettle on, polly put the kettle on, polly put the kettle on we’ll all have tea",
            "Polly", "1,26,51")]
        [TestCase("Polly put the kettle on, polly put the kettle on, polly put the kettle on we’ll all have tea", "ll",
            "3,28,53,78,82")]
        [TestCase("Polly put the kettle on, polly put the kettle on, polly put the kettle on we’ll all have tea", "X",
            "")]
        [TestCase("Polly put the kettle on, polly put the kettle on, polly put the kettle on we’ll all have tea", "Polx"
            , "")]
        [TestCase("Polly put the kettle on, polly put the kettle on, polly put the kettle on we’ll all have tea",
            "teacup", "")]
        public void GivenAnInstance_WhenPatternsAreMatchedMultiply_CorrectResultIsReturned(string text, string subtext,
                                                                                           string match)
        {
            var sut = new StringFinder(text);

            string result = string.Join(",", sut.Find(subtext));

            Assert.AreEqual(match, result);
        }

        [Test]
        [TestCase("abc", "a", 1)]
        [TestCase("abc", "b", 2)]
        [TestCase("abc", "d", 0)]
        [TestCase("abc", "bc", 2)]
        [TestCase("abc", "ba", 0)]
        public void GivenAnInstance_WhenPatternsAreMatchedOnce_CorrectResultIsReturned(string text, string subtext,
                                                                                       int match)
        {
            var sut = new StringFinder(text);

            int result = sut.Find(subtext).SingleOrDefault();

            Assert.AreEqual(match, result);
        }
    }
}