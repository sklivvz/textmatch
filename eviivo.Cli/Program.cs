﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eviivo.Core;

namespace eviivo.Cli
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Count() != 2)
            {
                Console.WriteLine(@"TextMatch

The application matches the subtext against the text, outputting the character 
positions of the beginning of each match for the subtext within the text. 

Usage: 

TextMatch <text> <subtext>

where the first argument should be the string to find matches within, and the 
second argument should be the string to search for.

Notes:

- Multiple matches are possible. 
- Matching is case insensitive. 
- If there are no matches, no output is generated.");
                Environment.Exit(-1);
            }

            var finder = new StringFinder(args[0]);
            var result = string.Join(",", finder.Find(args[1]));

            if (!string.IsNullOrWhiteSpace(result))
                Console.WriteLine(result);
        }
    }
}
